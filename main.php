<?php 

/*
 La fonction prend en entrée un tableau d'intervalles et retourne un tableau des intervalles fusionnés
*/
function foo(array $intervalleList): array {
    // On trie les intervalles par ordre croissant
    usort($intervalleList, fn($a, $b) => $a[0] <=> $b[0]);
    // Initialisation des variables
    $result = [];
    $prev = $intervalleList[0];

    foreach ($intervalleList as $interval) {
        if ($prev[1] >= $interval[0]) {
            $prev[1] = max($prev[1], $interval[1]);
        } else {
            $result[] = $prev;
            $prev = $interval;
        }
    }
    $result[] = $prev;
    return $result;
}

// Tests d'affichage
print_r(foo([[0, 3], [6, 10]])); // [[0, 3], [6, 10]]
print_r(foo([[0, 5], [3, 10]])); // [[0, 10]]
print_r(foo([[0, 5], [2, 4]])); // [[0, 5]]
print_r(foo([[7, 8], [3, 6], [2, 4]])); // [[2, 6], [7, 8]]
print_r(foo([[3, 6], [3, 4], [15, 20], [16, 17], [1, 4], [6, 10], [3, 6]])); // [[1, 10], [15, 20]]

?>